import subprocess as sp
import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

scale = "scale-to-fit"
scale_command = "dconf dump /sm/puri/phoc/ > scale.txt"
failure = f"Unable to save the contents of {scale_command}. So I am unable to parse the contents to know what apps are already to scale."
#to_scale = []
#scale_dump = sp.call(scale_command,shell=True)
#with open('scale.txt', 'rt') as file:
#    for line in file:
#        to_scale.append(line)
#print(to_scale)
try:
    #This is making an empty list that will store wether or not a app is scaled
    to_scale = []
    #This searches through the directory that stores the info then saves the output in a txt file.
    scale_dump = sp.call(scale_command, shell=True)
    #this is opening that txt file and storing the contents into the list
    with open('scale.txt', 'rt') as file:
        for line in file:
            to_scale.append(line)
    print(to_scale)

except:
    print(f"{failure}")

class gtk_scaling(Gtk.Window):
    def __init__(self):
        Gtk.Window.__init__(self, title="App Scaling on Mobian")
        self.set_border_width(13)
        grid = Gtk.Grid()
        self.add(grid)

        # Firefox Switch/Label
        firefox_label = Gtk.Label()
        firefox_label.set_text("Firefox scaling switch")
        firefox_switch = Gtk.Switch()
        # firefox_button = Gtk.Button(label="Firefox Scaling")
        firefox_switch.connect("notify::active", self.firefox_switching)
        if to_scale[0] == 'scale-to-fit=true\n':
            firefox_switch.set_active(True)
        else:
            firefox_switch.set_active(False)
        grid.attach(firefox_switch, 0, 1, 1, 1)
        grid.attach(firefox_label, 0, 0, 1, 1)

        # Switch for totem/label
        totem_label = Gtk.Label()
        totem_label.set_text("Totem scaling switch")
        totem_switch = Gtk.Switch()
        totem_switch.connect("notify::active", self.totem_switching)
        totem_switch.set_active(False)
        grid.attach(
            totem_switch, 1, 1, 1, 1
        )  # (button added, column, row, column span, row span)
        grid.attach(totem_label, 1, 0, 1, 1)

        # calendar switch/label
        calendar_label = Gtk.Label()
        calendar_label.set_text("Calendar scaling switch")
        calendar_switch = Gtk.Switch()
        calendar_switch.connect("notify::active", self.calendar_switching)
        grid.attach(calendar_switch, 0, 3, 1, 1)
        grid.attach(calendar_label, 0, 2, 1, 1)

        # cawbird switch/label
        cawbird_label = Gtk.Label()
        cawbird_label.set_text("Cawbird scaling switch")
        cawbird_switch = Gtk.Switch()
        cawbird_switch.connect("notify::active", self.cawbird_switching)
        grid.attach(cawbird_label, 1, 2, 1, 1)
        grid.attach(cawbird_switch, 1, 3, 1, 1)

        # Empathy switch/label
        empathy_label = Gtk.Label()
        empathy_label.set_text("Empathy scaling switch")
        empathy_switch = Gtk.Switch()
        empathy_switch.connect("notify::active", self.empathy_switching)
        grid.attach(empathy_label, 0, 4, 1, 1)
        grid.attach(empathy_switch, 0, 5, 1, 1)

        # Geary button
        geary_label = Gtk.Label()
        geary_label.set_text("Geary scaling switch")
        geary_switch = Gtk.Switch()
        geary_switch.connect("notify::active", self.geary_switching)
        grid.attach(geary_label, 1, 4, 1, 1)
        grid.attach(geary_switch, 1, 5, 1, 1)

        # Map switch/label
        map_label = Gtk.Label()
        map_label.set_text("Map scaling switch")
        map_switch = Gtk.Switch()
        map_switch.connect("notify::active", self.map_switching)
        grid.attach(map_label, 0, 6, 1, 1)
        grid.attach(map_switch, 0, 7, 1, 1)

        # mumble switch/label
        mumble_label = Gtk.Label()
        mumble_label.set_text("Mumble scaling switch")
        mumble_switch = Gtk.Switch()
        mumble_switch.connect("notify::active", self.mumble_switching)
        grid.attach(mumble_label, 1, 6, 1, 1)
        grid.attach(mumble_switch, 1, 7, 1, 1)

        # nautilus switch/label
        nautilus_label = Gtk.Label()
        nautilus_label.set_text("Nautilus scaling switch")
        nautilus_switch = Gtk.Switch()
        nautilus_switch.connect("notify::active", self.nautilus_switching)
        grid.attach(nautilus_label, 0, 8, 1, 1)
        grid.attach(nautilus_switch, 0, 9, 1, 1)

        # Sudoku switch/label
        sudoku_label = Gtk.Label()
        sudoku_label.set_text("Sudoku scaling switch")
        sudoku_switch = Gtk.Switch()
        sudoku_switch.connect("notify::activity", self.sudoku_switching)
        grid.attach(sudoku_label, 1, 8, 1, 1)
        grid.attach(sudoku_switch, 1, 9, 1, 1)

        # Dark theme button
        dark_theme_button = Gtk.Button(label="Dark Theme")
        dark_theme_button.connect("clicked", self.enable_dark_theme)
        # hbox.pack_start(dark_theme_button, True, True, 0)
        grid.attach(dark_theme_button, 0, 10, 2, 2)

        # Light theme button
        light_theme_button = Gtk.Button(label="Light Theme")
        light_theme_button.connect("clicked", self.enable_light_theme)
        grid.attach(light_theme_button, 0, 12, 2, 2)


        # close/exit button
        close_button = Gtk.Button(label="CLOSE!!!")
        close_button.connect("clicked", self.close_app)
        grid.attach(close_button, 0, 14, 2, 6)

        #Enable Zram/button Still working on how to do this.
        #zram_button = Gtk.Button(label="Enable Zram")
        #zram_button.connect("clicked", self.enable_zram)
        #grid.attach(zram_button, 0, 12, 2, 2)


    def firefox_scaling(self, widget):
        sp.call(f"{scale} firefox-esr on", shell=True)
        print(f"Ran the function {scale} firefox-esr on")

    def firefox_switching(self, switch, gparam):
        if switch.get_active():
            sp.call(f"{scale} firefox-esr on", shell=True)
        else:
            sp.call(f"{scale} firefox-ers off", shell=True)

    def totem_scaling(self, widget):
        sp.call(f"{scale} totem on", shell=True)
        print("Totem should be scaled properly")

    def totem_switching(self, switch, gparam):
        if switch.get_active():
            sp.call(f"{scale} totem on", shell=True)
        else:
            sp.call(f"{scale} totem off", shell=True)

    def calendar_scale(self, widget):
        sp.call(f"{scale} gnome-calendar on", shell=True)

    def calendar_switching(self, switch, gparam):
        if switch.get_active():
            sp.call(f"{scale} gnome-calendar on", shell=True)
        else:
            sp.call(f"{scale} gnome-calendar off", shell=True)

    def cawbird_scale(self, widget):
        sp.call(f"{scale} cawbird on", shell=True)

    def cawbird_switching(self, switch, gparam):
        if switch.get_active():
            sp.call(f"{scale} cawbird on", shell=True)
        else:
            sp.call(f"{scale} cawbird off", shell=True)

    def empathy_scale(self, widget):
        sp.call(f"{scale} empathy on", shell=True)
        pass

    def empathy_switching(self, switch, gparam):
        if switch.get_active():
            sp.call(f"{scale} empathy on", shell=True)
        else:
            sp.call(f"{scale} empathy off", shell=True)

    def geary_scale(self, widget):
        sp.call(f"{scale} geary on", shell=True)
        pass

    def geary_switching(self, switch, gparam):
        if switch.get_active():
            sp.call(f"{scale} geary on", shell=True)
        else:
            sp.call(f"{scale} geary off", shell=True)

    def map_scale(self, widget):
        sp.call(f"{scale} org.gnome.Maps on", shell=True)
        pass

    def map_switching(self, switch, gparam):
        if switch.get_active():
            sp.call(f"{scale} org.gnome.Maps on", shell=True)
        else:
            sp.call(f"{scale} org.gnome.Maps off", shell=True)

    def mumble_scale(self, widget):
        sp.call(f"{scale} net.sourceforge.mumble.mumble on", shell=True)
        pass

    def mumble_switching(self, switch, gparam):
        if switch.get_active():
            sp.call(f"{scale} net.sourceforge.mumble.mumble on", shell=True)
        else:
            sp.call(f"{scale} net.sourceforge.mumble.mumble off", shell=True)

    def nautilus_scale(self, widget):
        sp.call(f"{scale} org.gnome.nautilus on", shell=True)
        pass

    def nautilus_switching(self, switch, gparam):
        if switch.get_active():
            sp.call(f"{scale} org.gnome.nautilus on", shell=True)
        else:
            sp.call(f"{scale} org.gnome.nautilus off", shell=True)

    def sudoku_scale(self, widget):
        sp.call(f"{scale} gnome-sudoku on", shell=True)
        pass

    def sudoku_switching(self, switch, gparam):
        if switching.get_active():
            sp.call(f"{scale} gnome-sudoku on", shell=True)
        else:
            sp.call(f"{scale} gnome-sudoku off", shell=True)

    def enable_dark_theme(self, widget):
        dark_theme = (
            "gsettings set org.gnome.desktop.interface gtk-theme 'Adwaita-dark'"
        )
        sp.call(f"{dark_theme}", shell=True)
        pass

    def enable_light_theme(self, widget):
        light_theme = (
            "gsettings set org.gnome.desktop.interface gtk-theme 'Adwaita'"
        )
        sp.call(f"{light_theme}", shell=True)
        pass

    def close_app(self, widget):
        window.destroy()
        return

    #def enable_zram(self, widget):
        #self.username = Gtk.Entry()
        #self.username.set_text("Username")
        #self.password = Gtk.Entry()
        #self.password.set_text("Password")
        #self.password.set_visibility(False)
        #sp.call("sudo apt install zram-tools")
        #sp.call(self.password_get())


window = gtk_scaling()
window.connect("destroy", Gtk.main_quit)
window.show_all()
Gtk.main()
